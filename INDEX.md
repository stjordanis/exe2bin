# exe2bin

Convert an exe file to bin format


## Contributions

NLS specific corrections, updates and submissions should not be 
directly to submitted this project. NLS is maintained at the [FD-NLS](https://github.com/shidel/fd-nls)
project on GitHub. If the project is still actively maintained by it's
developer, it may be beneficial to also submit changes to them directly.

## EXE2BIN.LSM

<table>
<tr><td>title</td><td>exe2bin</td></tr>
<tr><td>version</td><td>1.5a</td></tr>
<tr><td>entered&nbsp;date</td><td>2006-07-24</td></tr>
<tr><td>description</td><td>Convert an exe file to bin format</td></tr>
<tr><td>keywords</td><td>freedos</td></tr>
<tr><td>author</td><td>ramax@maks.bhg.ru</td></tr>
<tr><td>maintained&nbsp;by</td><td>ramax@maks.bhg.ru</td></tr>
<tr><td>platforms</td><td>dos</td></tr>
<tr><td>copying&nbsp;policy</td><td>[Sybase Open Watcom Public License, Version 1.0](LICENSE)</td></tr>
<tr><td>wiki&nbsp;site</td><td>http://wiki.freedos.org/wiki/index.php/Exe2bin</td></tr>
</table>
